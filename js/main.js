function mainCtrl(){
    const doc = document;
    doc.getElementById('showMenu').addEventListener('click', showMenu);
}

function showMenu(){
    const doc = document;
    doc.getElementById('layerMenu').classList.add('active');
    doc.querySelector('.menu-container').classList.add('active');
    replaceShowMenu();
}

function replaceShowMenu(){
    const doc = document, 
          newSpan = doc.createElement('span'),
          oldSpan = doc.getElementById('showMenu'),
          elementParent = doc.querySelector('.main-header');
    newSpan.id = 'unshowMenu';
    newSpan.className = 'menu-hamburger';
    newSpan.innerHTML = '<i class="fas fa-bars fa-lg menu-icon"></i>';

    elementParent.replaceChild(newSpan, oldSpan);
    doc.getElementById('unshowMenu').addEventListener('click', unshowMenu);
}

function unshowMenu(){
    const doc = document, 
          newSpan = doc.createElement('span'),
          oldSpan = doc.getElementById('unshowMenu'),
          elementParent = doc.querySelector('.main-header');
    newSpan.id = 'showMenu';
    newSpan.className = 'menu-hamburger';
    newSpan.innerHTML = '<i class="fas fa-bars fa-lg menu-icon"></i>';

    elementParent.replaceChild(newSpan, oldSpan);

    doc.getElementById('layerMenu').classList.remove('active');
    doc.querySelector('.menu-container').classList.remove('active');
    doc.getElementById('showMenu').addEventListener('click', showMenu);
}

mainCtrl();